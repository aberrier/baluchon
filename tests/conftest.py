import os
from pathlib import Path

import pytest
from environs import Env
from baluchon.database import Database
from baluchon.source import Source, SQLFolderSource

env = Env()
env.read_env()


class TestDatabase(Database):
    TEST_DATABASE_PREFIX = "test_"

    def __init__(self):
        dsn = {
            "database": env("CLICKHOUSE_DATABASE"),
            "host": env("CLICKHOUSE_HOST"),
            "port": env("CLICKHOUSE_PORT"),
            "user": env("CLICKHOUSE_USER"),
            "password": env("CLICKHOUSE_PASSWORD"),
            "compression": env.bool("CLICKHOUSE_COMPRESSION", True),
        }
        config = {}
        self.original_db_name = dsn["database"]
        self.test_db_name = f"{self.TEST_DATABASE_PREFIX}{dsn['database']}"
        super().__init__(config=config, dsn=dsn)

    def create_test_database(self):
        assert self.test_db_name.startswith(self.TEST_DATABASE_PREFIX)
        self.run("CREATE DATABASE IF NOT EXISTS `%s`" % self.test_db_name)

        self.db_name = self.test_db_name
        self.dsn["database"] = self.db_name

    def drop_test_database(self):
        assert self.test_db_name.startswith(self.TEST_DATABASE_PREFIX)
        self.run("DROP DATABASE IF EXISTS `%s`" % self.test_db_name)

        self.db_name = self.original_db_name
        self.dsn["database"] = self.original_db_name


@pytest.fixture
def test_database():
    database = TestDatabase()
    database.open()
    database.create_test_database()
    yield database
    database.drop_test_database()


@pytest.fixture
def sql_folder_source(test_database):
    config = {
        "folder": str(
            Path(os.path.dirname(Path(__file__)))
            / "fixtures"
            / "sources"
            / "sql_folder"
        ),
        "context": {"DEFAULT_DATABASE": test_database.db_name},
    }
    source = SQLFolderSource(config=config)
    return source


def pytest_unconfigure(config):
    database = TestDatabase()
    database.open()
    database.drop_test_database()
