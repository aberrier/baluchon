CREATE TABLE {{ DEFAULT_DATABASE }}.b (
    name String,
    gender UInt8,
    birth DateTime
) engine = MergeTree()
        PRIMARY KEY (name)
        ORDER BY (name, gender)
        SETTINGS index_granularity = 8192;