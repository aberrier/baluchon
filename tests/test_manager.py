import random

import pytest
from baluchon.constants import EMPTY_VERSION, DEFAULT_MIGRATIONS_TABLE
from baluchon.exceptions import (
    NoChangeException,
    ShortLimitException,
    InvalidVersionException,
)
from baluchon.manager import MigrationManager


def test_migration_up(sql_folder_source, test_database):
    manager = MigrationManager(source=sql_folder_source, database=test_database)
    manager.up()
    assert manager.version() == (5, False)
    assert set(
        x for x, in test_database.run(f"SHOW TABLES FROM {test_database.db_name};")
    ) == {DEFAULT_MIGRATIONS_TABLE, "a", "b", "c", "d", "e"}


def test_migration_down(sql_folder_source, test_database):
    manager = MigrationManager(source=sql_folder_source, database=test_database)
    manager.up()
    assert manager.version() == (5, False)
    manager.down()
    assert manager.version() == (EMPTY_VERSION, False)
    assert set(
        x for x, in test_database.run(f"SHOW TABLES FROM {test_database.db_name};")
    ) == {DEFAULT_MIGRATIONS_TABLE}


def test_migration_double_up(sql_folder_source, test_database):
    manager = MigrationManager(source=sql_folder_source, database=test_database)
    manager.up()
    with pytest.raises(NoChangeException):
        manager.up()


def test_migration_down_empty_database(sql_folder_source, test_database):
    manager = MigrationManager(source=sql_folder_source, database=test_database)
    with pytest.raises(NoChangeException):
        manager.down()


def test_migration_steps_down(sql_folder_source, test_database):
    manager = MigrationManager(source=sql_folder_source, database=test_database)
    manager.up()
    manager.steps(-1)
    assert set(
        x for x, in test_database.run(f"SHOW TABLES FROM {test_database.db_name};")
    ) == {DEFAULT_MIGRATIONS_TABLE, "a", "b", "c", "d"}
    manager.steps(-2)
    assert set(
        x for x, in test_database.run(f"SHOW TABLES FROM {test_database.db_name};")
    ) == {DEFAULT_MIGRATIONS_TABLE, "a", "b"}
    with pytest.raises(NoChangeException):
        manager.steps(0)

    manager.steps(-3)
    assert set(
        x for x, in test_database.run(f"SHOW TABLES FROM {test_database.db_name};")
    ) == {DEFAULT_MIGRATIONS_TABLE}

    with pytest.raises(NoChangeException):
        manager.steps(-1)


def test_migration_steps_up(sql_folder_source, test_database):
    manager = MigrationManager(source=sql_folder_source, database=test_database)
    manager.steps(1)
    assert set(
        x for x, in test_database.run(f"SHOW TABLES FROM {test_database.db_name};")
    ) == {DEFAULT_MIGRATIONS_TABLE, "a"}
    manager.steps(2)
    assert set(
        x for x, in test_database.run(f"SHOW TABLES FROM {test_database.db_name};")
    ) == {DEFAULT_MIGRATIONS_TABLE, "a", "b", "c"}
    with pytest.raises(NoChangeException):
        manager.steps(0)
    with pytest.raises(ShortLimitException):
        manager.steps(5)
    manager.steps(2)
    assert set(
        x for x, in test_database.run(f"SHOW TABLES FROM {test_database.db_name};")
    ) == {DEFAULT_MIGRATIONS_TABLE, "a", "b", "c", "d", "e"}

    with pytest.raises(NoChangeException):
        manager.steps(1)


def test_migration_force(sql_folder_source, test_database):
    manager = MigrationManager(source=sql_folder_source, database=test_database)
    version = random.randint(1, 100)
    manager.force(version=version)
    assert manager.version() == (version, False)


def test_migration_force_invalid(sql_folder_source, test_database):
    manager = MigrationManager(source=sql_folder_source, database=test_database)
    version = random.randint(-100, -2)
    with pytest.raises(InvalidVersionException):
        manager.force(version=version)
    assert manager.version() == (EMPTY_VERSION, False)
